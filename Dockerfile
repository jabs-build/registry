FROM node:12 AS ts-build

WORKDIR /usr/src/jabs-registry
COPY package.json yarn.lock tsconfig.json ./
RUN yarn install
COPY src/ ./src
RUN yarn build


FROM node:12

WORKDIR /usr/lib/jabs-registry
COPY package.json yarn.lock ./
RUN yarn install --production
COPY --from=ts-build /usr/src/jabs-registry/lib ./lib

VOLUME [ "/packages" ]
EXPOSE 3000
CMD [ "yarn", "start" ]