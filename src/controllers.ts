import { RequestHandler } from "express-serve-static-core";

import Package from "./model/package";
import Version from "./model/version";
import semver from "semver";
import fs, { promises as pfs } from "fs";

const getRegistryMetadata: RequestHandler = async (req, res) => {
	const packages = await Package.find({}, "name").exec();
	res.json({
		registry: "local",
		packages: packages.map(p => p.name)
	});
};

const getPackageMetadata: RequestHandler = async (req, res) => {
	const requestedPackage = req.params.package;
	const pkg = await Package.findOne({ name: requestedPackage }).exec();
	if (!pkg)
		return res.status(404).send("Package not found");
	const latestVersion = pkg.latest
		&& await Version.findById(pkg.latest).exec();
	const versions = await Version.find({ package: pkg }, "version").exec();
	console.log(versions);

	res.json({
		name: pkg.name,
		latest: latestVersion && latestVersion.version,
		versions: versions.filter(v => !v.yanked).map(e => e.version)
	});
};

const getVersionMetadata: RequestHandler = async (req, res) => {
	const requestedPackage = req.params.package;
	const requestedVersion = req.params.version;
	const pkg = await Package.findOne({ name: requestedPackage }).exec();
	if (!pkg)
		return res.status(404).send("Package not found");

	const packageVersions = await Version.find({ package: pkg }, "version").exec();
	const max = semver.maxSatisfying(packageVersions.map(v => v.version), requestedVersion);

	if (!max)
		return res.status(404).send("No version satisfies specified bounds");

	res.json({
		name: pkg.name,
		requested: requestedVersion,
		version: max
	});
};

const fetchVersionTar: RequestHandler = async (req, res) => {
	const requestedPackage = req.params.package;
	const requestedVersion = req.params.version;

	const pkg = await Package.findOne({ name: requestedPackage }).exec();
	if (!pkg)
		return res.status(404).send("Package not found");

	const packageVersions = await Version.find({ package: pkg }, "version").exec();
	const max = semver.maxSatisfying(packageVersions.map(v => v.version), requestedVersion);

	if (!max)
		return res.status(404).send("No version satisfies specified bounds");

	res.contentType("application/gzip");
	res.sendFile(`/packages/${pkg.name}/${pkg.name}-${max}.tar.gz`); // Path to docker volume
};

const pushVersionTar: RequestHandler = async (req, res) => {
	const requestedPackage = req.params.package;
	const requestedVersion = req.params.version;

	const pkg = await Package.findOne({ name: requestedPackage }).exec();
	if (!pkg)
		return res.status(404).send("Package not found");

	const parsedVersion = semver.valid(requestedVersion);
	if (!parsedVersion)
		return res.status(400).send("Invalid version");

	if (await Version.findOne({ package: pkg, version: parsedVersion }))
		return res.status(409).send("Version already exists");

	const body = req.body;
	if (!body.data)
		return res.status(400).send("Body is missing \"data\" field");

	const dataBuffer = Buffer.from(body.data, "base64");
	await pfs.mkdir(`/packages/${pkg.name}`);
	await pfs.writeFile(`/packages/${pkg.name}/${pkg.name}-${parsedVersion}.tar.gz`, dataBuffer);

	const newVersionEntry = new Version({
		version: parsedVersion,
		package: pkg
	});
	await newVersionEntry.save();

	if (!pkg.latest || semver.lt(pkg.latest, parsedVersion)) {
		pkg.latest = newVersionEntry;
		await pkg.save();
	}

	res.send("OK");
};

const createPackage: RequestHandler = async (req, res) => {
	const requestedPackage = req.params.package;

	if (await Package.findOne({ name: requestedPackage }).exec())
		return res.status(409).send("Package already exists");

	const newPackage = new Package({
		name: requestedPackage
	});
	await newPackage.save();
	res.send("OK");
};

export {
	getRegistryMetadata,
	getPackageMetadata,
	getVersionMetadata,
	fetchVersionTar,
	pushVersionTar,
	createPackage
};