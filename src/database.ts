import mongoose from "mongoose";

export function connectToDatabase() {
	return mongoose.connect(process.env.DATABASE_URL || "mongodb://localhost/jabs-registry", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
}