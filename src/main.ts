import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";

import {
	getRegistryMetadata,
	getPackageMetadata,
	getVersionMetadata,
	fetchVersionTar,
	pushVersionTar,
	createPackage
} from "./controllers";
import { connectToDatabase } from "./database";

const app = express();
const port = 3000;

async function main() {
	app.use(cors());
	app.use(bodyParser.json());
	app.use(morgan("common"));

	app.get("/", getRegistryMetadata);
	app.get("/:package", getPackageMetadata);
	app.get("/:package/:version", getVersionMetadata);
	app.get("/:package/:version/tgz", fetchVersionTar);
	app.put("/:package/:version", pushVersionTar);
	app.put("/:package", createPackage);

	await connectToDatabase();
	app.listen(process.env.JABS_REGISTRY_PORT || port,
		() => console.log(`JABS Registry is running on port ${port}`));
}

// tslint:disable-next-line no-floating-promises
main();