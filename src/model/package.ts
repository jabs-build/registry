import mongoose, { Schema, Document, Mongoose } from "mongoose";
import { IVersion } from "./version";
const { Types } = Schema;

export interface IPackage extends Document {
	name: string;
	latest: IVersion["_id"] | null;
}

const PackageSchema = new Schema({
	name: { type: Types.String, required: true, unique: true },
	latest: { type: Types.ObjectId, ref: "Version", default: null }
});

export default mongoose.model<IPackage>("Package", PackageSchema);