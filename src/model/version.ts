import mongoose, { Schema, Document, Mongoose } from "mongoose";
import { IPackage } from "./package";
const { Types } = Schema;

export interface IVersion extends Document {
	version: string;
	package: IPackage["_id"];
	yanked: boolean;
}

const VersionSchema = new Schema({
	version: { type: Types.String, required: true, unique: true },
	package: { type: Types.ObjectId, ref: "Package", required: true },
	yanked: { type: Types.Boolean, default: false }
});

export default mongoose.model<IVersion>("Version", VersionSchema);